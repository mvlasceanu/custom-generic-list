/**
 * Mihai-Marius Vlăsceanu
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @package    dk.itu.oop.reexam.task3
 * @class      MyEmptyList
 * @date       Jul 5, 2014
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 * @copyright  Copyright (c) 2014  Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 */
package dk.itu.oop.reexam.task3;

import java.util.ArrayList;
import java.util.List;

public final class MyEmptyList<T> extends MyGenericListAbstract<T> {
    
    private final List<T> list = new ArrayList<>();
    
    @Override
    public final T getLastElement() {
        return this.getList().get(this.size() - 1);
    }

    @Override
    public final T getHead()
    {
        if(this.size() >= 0)
            return this.getList().get(0);
        else
            return null;
    }

    public void add(T e)
    {
        this.getList().add(e);
        this.updateSize();
    }
    
    public final T[] getTail()
    {
        T[] local = null;
        
        for(int i = 0; i < this.size(); i++)
        {
            if(i > 0)
                local[i] = this.getTail()[i];
        }
        
        return local;
    }
    
    public final List<T> getList()
    {
        return this.list;
    }
    
    private void updateSize()
    {
        this.size = this.getList().size();
    }
    
    @Override
    public final void concat(T l)
    {
        this.add(l);
    }
    
    @Override
    public final void remove()
    {
        if(this.getList().size() > 0)
        {
            this.getList().remove(0);
            this.updateSize();
        }
    }
}
