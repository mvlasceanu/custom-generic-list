/**
 * Mihai-Marius Vlăsceanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the file LICENSE.txt. It is also available
 * through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @package dk.itu.oop.reexam.task3
 * @class Main
 * @date Jul 5, 2014
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License
 * (OSL 3.0)
 * @author Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 * @copyright Copyright (c) 2014 Mihai-Marius Vlăsceanu
 * <mihaivlasceanu@gmail.com>
 */
package dk.itu.oop.reexam.task3;

/**
 *
 * @author Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MyGenericListAbstract <Integer> list0   = new MyEmptyList<>(); // HERE YOU CREATE AN EMPTY LIST
        list0.add(3);
        MyGenericListAbstract <Integer> list1   = new MyNonEmptyList<>(12, list0);
        MyGenericListAbstract <Integer> list2   = new MyNonEmptyList<>(34, list1);
        list2.add(9);
        System.out.println("list1 is: " + list1);
        System.out.println("list2 is: " + list2);
        System.out.println("list 2 has this number of elements: " + list2.size());
        System.out.println("list2 first element is: " + list2.getHead());
        System.out.println("list2 last element is: " + list2.getLastElement());
        list0.add(list1);
        System.out.println("list2 is: " + list2);
        // HERE YOU TEST YOUR ADD METHOD
    }
    
}
