/**
 * Mihai-Marius Vlăsceanu
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @package    dk.itu.oop.reexam.task3
 * @class      MyNonEmptyList
 * @date       Jul 5, 2014
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 * @copyright  Copyright (c) 2014  Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 */
package dk.itu.oop.reexam.task3;

import java.util.ArrayList;

public final class MyNonEmptyList<T, U> extends MyGenericListAbstract<T> {
    protected ArrayList<T> tail_elements = new ArrayList<>();
    public MyNonEmptyList(T head, U tail)
    {
        this.setHead(head);
        this.add((T) tail);
    }
    
    public T getTail() {
        return (T) this.tail;
    }
    
    private ArrayList<T> getTailElements()
    {
        return this.tail_elements;
    }
    
    @Override
    public void add(T e)
    {
        if(e instanceof dk.itu.oop.reexam.task3.MyNonEmptyList)
        {
            MyNonEmptyList enel = (MyNonEmptyList) e;
            T enHead = (T) enel.getHead();
            this.getTailElements().add(enHead);
            
            for(Object tElem : enel.getTailElements())
            {
                T tailElement = (T) tElem;
                
                this.getTailElements().add(tailElement);
            }
        } else if (e instanceof dk.itu.oop.reexam.task3.MyEmptyList ) {
            MyEmptyList list = (MyEmptyList) e;
            if(list.size() == 0)
                return;
            
            for(Object el : list.getList())
            {
                this.getTailElements().add((T) el);
            }
        } else {
            this.getTailElements().add(e);
        }

        this.updateSize();
    }
    
    private void updateSize()
    {
        int local_size = 0;
        if(this.getHead() != null)
           local_size++;
        
        if(!this.getTailElements().isEmpty())
            local_size = local_size + this.getTailElements().size();
        
        this.size = local_size;
    }
    
    @Override
    public T getLastElement()
    {
        int tail_elements_count = this.getTailElements().size();
        if(tail_elements_count == 0)
            return (T) this.getHead();
        int last_element_idx = tail_elements_count - 1;
        
        T lElem = this.getTailElements().get(last_element_idx);

        return lElem;
    }
    
    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        output.append('[');
        output.append(this.getHead());
        
        if(this.getTailElements().size() > 0)
        {
            output.append(", [");
            
            for(Object obj : this.getTailElements())
            {
                output.append(obj);
                
                if(this.getTailElements().indexOf(obj) != this.getTailElements().size() - 1)
                {
                    output.append(',');
                } else {
                    output.append(']');
                }
            }
        }
        output.append(']');
        
        return output.toString();
    }
    
    public final void concat(T l)
    {
        this.add(l);
    }
    
    public final void remove()
    {
        if(this.getTailElements().size() > 0)
        {
            T ftl = (T) this.getTailElements().get(0);
            
            this.setHead(ftl);
            
            this.getTailElements().remove(ftl);
            
            this.updateSize();
        } else {
            this.setHead(null);
            this.updateSize();
        }
    }
}
