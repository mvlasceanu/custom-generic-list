/**
 * Mihai-Marius Vlăsceanu
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * @package    dk.itu.oop.reexam.task3
 * @class      MyGenericListAbstract
 * @date       Jul 5, 2014
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 * @copyright  Copyright (c) 2014  Mihai-Marius Vlăsceanu <mihaivlasceanu@gmail.com>
 */
package dk.itu.oop.reexam.task3;

public abstract class MyGenericListAbstract<T> {
    protected transient T head;
    protected transient T tail;
    protected transient int size;
    
    public T getHead() {
        return (T) this.head;
    }

    protected final void setHead(T n)
    {
        this.head = n;
    }
    
    public abstract T getLastElement();
    
    public final int size() {
        return this.size;
    }
    
    public abstract void add(T e);
    
    public abstract void remove();
    
    public abstract void concat(T li);

}
